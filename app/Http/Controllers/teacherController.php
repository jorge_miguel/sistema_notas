<?php

namespace App\Http\Controllers;
use App\Model\teacher;
use Illuminate\Http\Request;

class teacherController extends Controller
{
    public function index(){
        return teacher::all();
    }
    public function store(Request $request){
        $teacher = new teacher();
        $teacher->fill($request->toArray())->save();
        return $teacher;
    }
    public function destroy($id){
        $teacher = teacher::find($id);
        $teacher->delete();
    }
}
