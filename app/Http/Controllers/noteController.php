<?php

namespace App\Http\Controllers;
use App\Model\note;
use Illuminate\Http\Request;

class noteController extends Controller
{
    public function index(){
        return note::all();
    }
    public function store(Request $request){
        $note = new note();
        $note->fill($request->toArray())->save();
        return $note;
    }
    public function destroy($id){
        $note = note::find($id);
        $note->delete();
    }
    
}
