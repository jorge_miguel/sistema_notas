<?php

namespace App\Http\Controllers;
use App\Model\matter;
use Illuminate\Http\Request;

class matterController extends Controller
{
    public function index(){
        return matter::all();
    }
    public function store(Request $request){
        $matter = new matter();
        $matter->fill($request->toArray())->save();
        return $matter;
    }
    public function destroy($id){
        $matter = matter::find($id);
        $matter->delete();
    }
}
