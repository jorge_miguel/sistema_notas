<?php

namespace App\Http\Controllers;
use App\Model\student;
use Illuminate\Http\Request;

class studentController extends Controller
{
    public function index(){
        return student::all();
    }
    public function store(Request $request){
        $student = new student();
        $student->fill($request->toArray())->save();
        return $student;
    }
    public function destroy($id){
        $student = student::find($id);
        $student->delete();
    }
}
