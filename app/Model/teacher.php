<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class teacher extends Model
{
    protected $table ='teacher';

    protected $primaryKey ='id_teach';
    public $TIMESTAMPS = true;
    const CREATED_AT = 'create_ad';
    const UPDATED_AT = 'update_ad';


    protected $fillabel =[
        'name_teach',
        'lastname_teach',
        'birthdate_teach',
        'age_teach',
        'email_teach',
        'pass_teach',
        'phone_teach',
        'create_ad',
        'update_ad'
    ]; 
}
