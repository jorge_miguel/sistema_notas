<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class matter extends Model
{
    protected $table ='matter';

    protected $primaryKey ='id_mett';
    public $TIMESTAMPS = true;
    const CREATED_AT = 'create_ad';
    const UPDATED_AT = 'update_ad';


    protected $fillabel =[
        'name_mett',
        'description_mett',
        'create_ad',
        'update_ad'
    ]; 
}
