<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class note extends Model
{
    protected $table ='note';

    protected $primaryKey ='id_note';
    public $TIMESTAMPS = true;
    const CREATED_AT = 'create_ad';
    const UPDATED_AT = 'update_ad';


    protected $fillabel =[
        '1p_note',
        '2p_note',
        '3p_note',
        'create_ad',
        'update_ad',
        'mett_id',
        'stud_id'
    ]; 
}
