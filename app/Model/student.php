<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class student extends Model
{
    protected $table ='student';

    protected $primaryKey ='id_stud';
    public $TIMESTAMPS = true;
    const CREATED_AT = 'create_ad';
    const UPDATED_AT = 'update_ad';


    protected $fillabel =[
        'name_stud',
        'lastname_stud',
        'birthdate_stud',
        'age_stud',
        'email_stud',
        'pass_stud',
        'phone_stud',
        'create_ad',
        'update_ad'
    ]; 
}
