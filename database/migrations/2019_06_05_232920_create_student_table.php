<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student', function (Blueprint $table) {
            $table->bigIncrements('id_stud');
            $table->string('name_stud')->nullabel(true);
            $table->string('lastname_stud')->nullabel(true);
            $table->date('birthdate_stud')->nullabel(true);
            $table->integer('age_stud')->nullabel(true);
            $table->string('email_stud')->nullabel(true);
            $table->string('pass_stud')->nullabel(true);
            $table->integer('phone_stud')->nullabel(true);
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student');
    }
}
