<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeacherTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teacher', function (Blueprint $table) {
            $table->bigIncrements('id_teach');
            $table->string('name_teach')->nullabel(true);
            $table->string('lastname_teach')->nullabel(true);
            $table->date('birthdate_teach')->nullabel(true);
            $table->integer('age_teach')->nullabel(true);
            $table->string('email_teach')->nullabel(true);
            $table->string('pass_teach')->nullabel(true);
            $table->integer('phone_teach')->nullabel(true);
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teacher');
    }
}
