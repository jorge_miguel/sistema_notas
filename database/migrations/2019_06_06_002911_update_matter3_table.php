<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateMatter3Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('note', function (Blueprint $table) {
            $table->unsignedBigInteger('mett_id');
        
            $table->foreign('mett_id')->references('id_mett')->on('matter');

            $table->unsignedBigInteger('stud_id');
        
            $table->foreign('stud_id')->references('id_stud')->on('student');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
