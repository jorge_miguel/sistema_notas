-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 06-06-2019 a las 02:34:21
-- Versión del servidor: 10.1.38-MariaDB
-- Versión de PHP: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `notes_sist`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `matter`
--

CREATE TABLE `matter` (
  `id_mett` bigint(20) UNSIGNED NOT NULL,
  `name_mett` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description_mett` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `teacher_mett` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `matter`
--

INSERT INTO `matter` (`id_mett`, `name_mett`, `description_mett`, `teacher_mett`, `created_at`, `updated_at`) VALUES
(1, 'estructura', 'ard', '', NULL, NULL),
(2, 'compilador', 'fgh', '', NULL, NULL),
(3, 'fgh', 'hgkjh', '', NULL, NULL),
(4, 'as', 'erty', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2019_06_05_232920_create_student_table', 1),
(2, '2019_06_05_232951_create_matter_table', 1),
(3, '2019_06_05_233015_create_teacher_table', 1),
(4, '2019_06_05_233209_create_note_table', 1),
(5, '2019_06_05_234618_update_estudent1_table', 1),
(6, '2019_06_05_235910_update_matter2_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `note`
--

CREATE TABLE `note` (
  `id_note` bigint(20) UNSIGNED NOT NULL,
  `1p_note` int(11) NOT NULL,
  `2do_note` int(11) NOT NULL,
  `3er_note` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `mett_id` bigint(20) UNSIGNED NOT NULL,
  `stud_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `note`
--

INSERT INTO `note` (`id_note`, `1p_note`, `2do_note`, `3er_note`, `created_at`, `updated_at`, `mett_id`, `stud_id`) VALUES
(1, 56, 65, 34, NULL, NULL, 2, 1),
(2, 1, 1, 1, NULL, NULL, 2, 3),
(3, 65, 45, 56, NULL, NULL, 3, 2),
(4, 23, 43, 45, NULL, NULL, 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `student`
--

CREATE TABLE `student` (
  `id_stud` bigint(20) UNSIGNED NOT NULL,
  `name_stud` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname_stud` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthdate_stud` date NOT NULL,
  `age_stud` int(11) NOT NULL,
  `email_stud` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pass_stud` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_stud` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `student`
--

INSERT INTO `student` (`id_stud`, `name_stud`, `lastname_stud`, `birthdate_stud`, `age_stud`, `email_stud`, `pass_stud`, `phone_stud`, `created_at`, `updated_at`) VALUES
(1, 'jorge', 'cervantes', '2019-06-11', 111, 'asd@asd.com', 'a1s2d3', 432, NULL, NULL),
(2, 'miguel', 'asd', '2019-06-26', 34, 'qwe@asd.com', 'a1s2d3', 432, NULL, NULL),
(3, 'jun', 'qwe', '2019-06-07', 34, 'ewr@gmail.com', 'a1s2d3', 9876, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `teacher`
--

CREATE TABLE `teacher` (
  `id_teach` bigint(20) UNSIGNED NOT NULL,
  `name_teach` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname_teach` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthdate_teach` date NOT NULL,
  `age_teach` int(11) NOT NULL,
  `email_teach` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pass_teach` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_teach` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `teacher`
--

INSERT INTO `teacher` (`id_teach`, `name_teach`, `lastname_teach`, `birthdate_teach`, `age_teach`, `email_teach`, `pass_teach`, `phone_teach`, `created_at`, `updated_at`) VALUES
(1, 'ytry', 'yoiup', '2019-06-05', 34, 'dfe@gmail.com', 'qwe', 435, NULL, NULL),
(2, 'ert', 'gdfg', '2019-06-21', 56, 'qwe@gamil.com', '124', 4253, NULL, NULL),
(3, 'tre', 'qwe', '2019-06-01', 45, 'wer@ewr.com', '34325s', 234234, NULL, NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `matter`
--
ALTER TABLE `matter`
  ADD PRIMARY KEY (`id_mett`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `note`
--
ALTER TABLE `note`
  ADD PRIMARY KEY (`id_note`),
  ADD KEY `note_mett_id_foreign` (`mett_id`),
  ADD KEY `note_stud_id_foreign` (`stud_id`);

--
-- Indices de la tabla `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id_stud`);

--
-- Indices de la tabla `teacher`
--
ALTER TABLE `teacher`
  ADD PRIMARY KEY (`id_teach`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `matter`
--
ALTER TABLE `matter`
  MODIFY `id_mett` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `note`
--
ALTER TABLE `note`
  MODIFY `id_note` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `student`
--
ALTER TABLE `student`
  MODIFY `id_stud` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `teacher`
--
ALTER TABLE `teacher`
  MODIFY `id_teach` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `note`
--
ALTER TABLE `note`
  ADD CONSTRAINT `note_mett_id_foreign` FOREIGN KEY (`mett_id`) REFERENCES `matter` (`id_mett`),
  ADD CONSTRAINT `note_stud_id_foreign` FOREIGN KEY (`stud_id`) REFERENCES `student` (`id_stud`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
